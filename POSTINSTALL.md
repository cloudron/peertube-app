This app is pre-setup with an admin account. The initial credentials are:

**Username**: root<br/>
**Password**: changeme<br/>

IMPORTANT: PeerTube does not support changing the location of the app after installation. Doing so, will
break the installation.

