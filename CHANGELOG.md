[0.1.0]
* Initial version

[0.2.0]
* Update logo

[0.3.0]
* Set min box version to 5.3.0

[0.4.0]
* Set trust proxy correctly
* Update PeerTube to 2.3.0
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v2.3.0)
* Add global search support (has to be explicitely enabled by admins)
* Add ability for admins to display a banner on their instance
* Support Vietnamese and Kabyle languages. Also re-establish Occitan language locale despite lack of support in Angular
* Add video miniature dropdown in Discover page
* Add channel information in My videos page
* Add videos count per channel in My channels page

[0.5.0]
* Fix issue where configs were not loaded correctly

[0.6.0]
* Install CLI tool in /app/code/cli

[0.7.0]
* Update Peertube to 2.4.0
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v2.4.0)
* Add global search support (has to be explicitely enabled by admins)
* Add ability for admins to display a banner on their instance
* Support Vietnamese and Kabyle languages. Also re-establish Occitan language locale despite lack of support in Angular
* Moderation:
  * Add ability to bulk delete comments of an account
  * Add ability to mute accounts from video miniature
  * Improve report modal: @rigelk in #2842
* Replaced softies icons by feather icons @rigelk
* Support player hotkeys when it is not focused
* Improve video miniature grids to fill the space as much as possible @rigelk
* Add video miniature dropdown in Discover page

[1.0.0]
* Fix transcoding issues

[1.1.0]
* Enable LDAP

[1.1.1]
* Fix transcoding error with readonly file system

[1.2.0]
* Update ffmpeg to 4.3

[1.2.0]
* Update to Peertube v3.0.0 RC1

[2.0.0]
* Update to Peertube v3.0.0
* Remove deprecated video abuse API
* Update the default configuration to not federate unlisted videos. We recommend to admins to update this [setting](https://github.com/Chocobozzz/PeerTube/blob/develop/config/production.yaml.example#L182)
* Update the default configuration to remove remote video views to reduce DB size and improve performances. We recommend to admins to update this [setting](https://github.com/Chocobozzz/PeerTube/blob/develop/config/production.yaml.example#L170)

[2.0.1]
* Update Peertube to 3.0.1
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v3.0.1)
* Security: Fix retrieving data of another user if the username contains _ when fetching my information
* Fix account feed URL
* Log RTMP server error (address already in use)
* Fix NPM theme links in admin theme page
* Don't reject AP actors with empty description

[2.0.2]
* Update base image to v3

[2.1.0]
* Update Peertube to 3.1.0
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v3.1.0)
* Fair transcoding jobs priority: give an higher priority to optimize jobs and decrease priority of transcoding jobs depending on the amount of videos uploaded by the user during the last 7 days #3637
* Allow admins to choose the transcoding jobs concurrency
* Support Albanian locale
* Async torrent creation on video upload. We hope that it should fix some weird upload errors
* Add .m4a audio upload support
* Stricter youtube-dl format selectors for import (don't import HDR videos and cap to the max supported resolution) #3516
* Don't publish imported videos before the user submitted the second step form
* Implement hot and best trending algorithms #3625 & #3681
* Support webp avatar upload
* Allow user to search through their watch history #3576
* Allow AP resolution for default account/channel pages (/accounts/:name/video-channels and /video-channels/:name/videos)

[2.2.0]
* Update Peertube to 3.2.1
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v3.2.0)
* By default, HLS transcoding is now enabled and webtorrent is disabled. We suggest you to reflect this change.
* PeerTube client now displays bigger video thumbnails.

[2.3.0]
* Update PeerTube to 3.3.0
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v3.3.0)

[2.4.0]
* Update PeerTube to 3.4.0
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v3.4.0)
* Add video filters to common video pages (account videos, channel videos, recently added/local/trending videos...)
* Add ability for instances to follow any actor (so specific accounts and channels)
* Updated HLS.js (library to play HLS playlists in PeerTube player) to V1:
* Add ability to search by PeerTube host in search filters
* Disallow search engine indexation of remote channels/accounts
* Support Latin language for videos

[2.4.1]
* Update PeerTube to 3.4.1
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v3.4.1)
* Fix broken PeerTube when cookies are disabled or if the embed iframe does not have appropriate options
* Fix search by channel's handle with an handle containing the local host
* Don't display autoblock message in upload page it is not enabled by the admin
* Don't index /about/peertube page
* Correctly handle OEmbed with an URL containing query parameters
* More robust youtube-dl thumbnail import
* Don't send a new video notification when using create transcoding CLI script

[2.5.0]
* Update PeerTube to 4.0.0
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v4.0.0)

[2.5.1]
* Update base image to 3.2.0

[2.6.0]
* Update PeerTube to 4.1.0
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v4.1.0)
* Player improvements
* Search improvements
* Video imports
* Add ability for users to delete individual elements in videos history
* Show date and views counter in playlist element miniature #4396
* Add norsk locale support
* Check mute status and display mute badges in channel and account pages
* Add No linguistic content video language option #4631
* Don't send notifications to admins/moderators if an admin/moderator reported an abuse
* Add ability for moderators/admins to edit any channel #4608
* Add a refresh button to admin videos overview page #4753

[2.6.1]
* Update PeerTube to 4.1.1
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v4.1.1)
* Strip EXIF data when processing images

[2.6.2]
* Patch CLI tool - see #4858

[2.6.3]
* Add prosody for livechat-plugin

[2.6.4]
* Symlink cache folder for node-gyp to work

[2.7.0]
* Update PeerTube to 4.2.0
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v4.2.0)

[2.7.1]
* Update PeerTube to 4.2.1
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v4.2.1)
* Fix live ending job that breaks new live session
* Fix search filters counter
* Fix upload banner icon margin
* Fix button icon margin
* Fix my import expander icon that should only be displayed on import error
* Fix select components styling inconsistency
* Increase max watch section to avoid too much warnings in server
* Optimize broadcast job creation
* Optimize View activities delivery using a dedicated broadcast job queue that can be run in parallel
* Fix video selection buttons placement
* Fix searching into account blocklist
* Fix incorrect instance stats
* Fix broken player on ICE error
* Relax views federation
* Fix peertube user in docker
* Fix playlist element federation with a deleted video

[2.7.2]
* Update PeerTube to 4.2.2
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v4.2.2)
* Upgrade vulnerable server dependencies
* Fix fast restream in permanent live
* Fix latency mode setting when creating a live
* Fix unique constraint tag violation when importing videos
* Fix latest live sessions order
* Fix server crash feed when accessing feeds that contains a live
* Fix false boolean attribute (data-is-live etc) in custom markup

[2.8.0]
* Update PeerTube to 4.3.0
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v4.3.0)
* Use yt-dlp by default instead of youtube-dl for new installations (because of much more dev activity)
* Reduce amount of PeerTube error logs
* Introduce log.log_tracker_unknown_infohash setting to disable "Unknown infoHash" warnings
* Web browsers send their error logs to the server that writes them in its own logs. Can be disabled by log.accept_client_log setting
* Enable metrics export using a Prometheus exporter
* Enable tracing export using a Jaeger exporter
* Automatically rebuild native plugin modules on NodeJS ABI change

[2.8.1]
* Update PeerTube to 4.3.1
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v4.3.1)
* Prevent XSS in sort select on pages that list videos. Thanks to Anthony Roth who reported the vulnerability!
* Fix broken embed player on live reload
* Fix channel follow when manually approve instance followers is enabled
* Fix input with number overflow on small screen
* Fix moderation dropdown overflow on mobile

[2.9.0]
* Update PeerTube to 5.0.0
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v5.0.0)
* Support object storage for live streams tada
* Support Two Factor authentication (OTP) tada
* Add explanation on disk space used for user quota admin config #5305
* Display channel in my videos list
* Show which playlists videos are added to in my videos list
* Add Channels link in left menu
* Add ... after the truncated video name in miniature
* Add object storage info badge in videos admin overview
* Add links to video files in videos admin overview
* Better indicate the live ended in embed by displaying a message and the live preview
* Force live autoplay by muting the video if necessary when the user was waiting for the live
* Handle network issues in video player #5138
* Cache chunks to upload in server to resume upload later #5224
* Add ability to serve custom static files under /.well-known URL path #5214
* Use account/channel avatar in account/channel RSS feeds #5325
* Add filter to sort videos by name #5351
* Add ability to configure OpenTelemetry Prometheus exporter listening hostname

[2.10.0]
* Update base image to 4.0.0

[2.10.1]
* Update PeerTube to 5.0.1
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v5.0.1)
* Fix HLS player infinite loading when the live stream/video ends
* Do not autoplay live without autoplay setting
* Fix private/internal video playback from Cloudflare object storage
* Fix local channel stats/OpenTelemetry metric
* Also display dropdown for videos from the homepage
* Fix broken P2P with live stream coming from object storage
* Fix responsive of table pagination

[2.10.2]
* Remove invokation of 5.0 upgade script

[2.11.0]
* Update PeerTube to 5.1.0
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v5.1.0)
* **IMPORTANT**: If your instance has signup enabled, user registration approval is automatically enabled by the default configuration of this release. You can change this setting in your production.yaml or in the configuration page in the web admin
Update web browsers support list:
  * Drop support of Safari 11 on iOS
  * Drop support of Safari 11 on desktop
  * Drop support of Firefox 68 on desktop
* Implement user registration approval (https://docs.joinpeertube.org/admin/managing-users#registration-approval) #5544
* Add "back to live" button in player
* Add Icelandic & Ukrainian locales
* Add Global views default trending algorithm option in admin configuration #5471

[2.12.0]
* Update PeerTube to 5.2.0
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v5.2.0)
* Implement remote transcoding for VOD videos, Live streams and Studio editions tada #5769
* If enabled, remote PeerTube runners can process these high CPU jobs
* Admin documentation: https://docs.joinpeertube.org/admin/remote-runners
* PeerTube runner CLI documentation: https://docs.joinpeertube.org/maintain/tools#peertube-runner
* Demonstration video: https://peertube2.cpy.re/w/oJwHHYwt4oKjKhLNh2diAY
* Architecture documentation: https://docs.joinpeertube.org/contribute/architecture#remote-vod-live-transcoding
* Add Podcast RSS feed support: #5487
* Add ability to set custom privacy for live replays #5692
* Render images of markdown fields in About page #5732
* Admin can disable user video history by default #5728
* Improve global accessibility

[2.12.1]
* Update PeerTube to 5.2.1
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v5.2.1)
* Fix loading spinner displayed forever on Chrome
* Fix broken replay with long live name
* Fix fps transcoding on remote runners
* Fix terms/code of conduct link toggle

[2.13.0]
* Update base image to 4.2.0

[2.14.0]
* Update PeerTube to 6.0.2
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.0.2)
* [Important breaking changes in 6.0](https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md#v600). Some keys in `production.yaml` have been renamed (for example, object storage keys).

[2.15.0]
* Migrate to OIDC login

[2.15.1]
* Update PeerTube to 6.0.3
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.0.3)
* Fix HTML meta tags with attributes that contain quotes
* Fix time parsing resulting in broken video start time in some cases
* Fix WebTorrent video import crash
* Reload Discover page on logout
* Fix privacy error when updating a live, even if the privacy has not changed
* Fix invalid remote live state change notification that causes the player to reload
* Don't apply big play button skin to settings menu
* Fix downloading video files from object storage with some video names (that include emojis, quotes etc)

[2.15.2]
* Update PeerTube to 6.0.4
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.0.4)
* Important: Prevent XSS injection in embed. Thanks Syst3m0ver!

[2.16.0]
* Update PeerTube to 6.1.0
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.1.0)
* Compact ActivityPub JSON-LD objects before using them to prevent incorrect access control @tesaguri
* Protect ActivityPub information related to private/internal/blocked videos

[2.17.0]
* Update PeerTube to 6.2.0
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.2.0)

[2.17.1]
* Update PeerTube to 6.2.1
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.2.1)
* Fix stuck runner jobs due to DB concurrency issue
* Respect OS orientation settings in PWA
* Fix "No results" not displayed on no video results
* Do not display "Download" option on lives
* Fix invalid current password error when updating user password
* Fix slow hotkeys detection
* Fix hidden runner jobs tab when remote runner is only enabled for transcription
* Fix broken HLS P2P by correctly updating HLS infohash on privacy update
* Fix videos filters pastille labels for categories and languages

[3.0.0]
* After the update to refresh metadata: run the `migrate` command in a webterminal into the app.
* Update PeerTube to 6.3.0
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.3.0)

[3.0.1]
* Update PeerTube to 6.3.1
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.3.1)
* Fix player settings button on mobile
* Fix removed audio when splitting audio and video streams on existing videos when running HLS transcoding

[3.0.2]
* Update PeerTube to 6.3.2
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.3.2)
* Fix 403 error when downloading private/internal video
* Don't crash video federation and live replay generation on missing thumbnail/preview
* Fix advanced search input with multiple automatic search tokens
* Fix player "Copy URL" when the video is fullscreen
* Fix account videos search
* Add missing max transcoding fps config in admin
* Don't add mobile buttons if the player controls are disabled

[3.0.3]
* Update PeerTube to 6.3.3
* [Full changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v6.3.3)
* Fix broken thumbnails on live replay
* Fix detecting portrait rotation of some video
* Don't allow to select a frame from a live to set the thumbnail
* Fix lost video stream with specific transcoding settings and video input
* Fix creating playlist without thumbnail when using the REST API
* Fix .mov video upload on some Windows versions
* Fix video-plugin-metadata.result client plugin hook

[4.0.0]
* Update PeerTube to 7.0.0
* [Full Changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v7.0.0)
* There are several changes in this major release. Please read upstream changelog
* Ensure `instance.default_client_route` (in web admin -> `Configuration` -> `Basic` -> `Landing page`) has a correct path: `/videos/trending`, `/videos/local` and `/videos/recently-added` have been removed in favour of `/videos/browse`
* Add ability to configure STUN servers IPs: `webrtc.stun_servers`
* Remove `client.videos.miniature.display_author_avatar` config: author avatars are now always displayed
* Improve accessibility:
* Add Slovakian language support to the client

[4.0.1]
* checklist added to CloudronManifest
* CLOUDRON_OIDC_PROVIDER_NAME implemented

[4.0.2]
* Update PeerTube to 7.0.1
* [Full Changelog](https://github.com/Chocobozzz/PeerTube/releases/tag/v7.0.1)
* Update translations
* Fix banner/avatar edit buttons
* Fix banner margin in channels page
* Textarea font size consistency
* Fix subscribe button radius
* Fix channel avatar info username
* Fix maximized markdown textarea
* Remove confusing channel message in *My playlists* pages
* Fix broken infinite scroll when deleting items (Videos, Channels...)

