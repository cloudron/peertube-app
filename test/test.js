#!/usr/bin/env node

/* global describe */
/* global before */
/* global after */
/* global afterEach */
/* global it */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    safe = require('safetydance'),
    util = require('util'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD || !process.env.EMAIL) {
    console.log('USERNAME, EMAIL and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TEST_TIMEOUT, 10) || 30000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app, cloudronName;
    let host_os;
    let username = process.env.USERNAME;
    let password = process.env.PASSWORD;
    let email = process.env.EMAIL;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        let inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
        const tmp = execSync(`cloudron exec --app ${app.id} env`).toString().split('\n').find((l) => l.indexOf('CLOUDRON_OIDC_PROVIDER_NAME=') === 0);
        if (tmp) cloudronName = tmp.slice('CLOUDRON_OIDC_PROVIDER_NAME='.length);
    }

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    async function getOS() {
        if (typeof(host_os) == 'undefined' || host_os == null)
            host_os = String(await execSync('uname -s')).trim();
        return host_os;
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    async function login(username, password) {
        await browser.get('https://' + app.fqdn + '/login');

        await waitForElement(By.id('username'));
        await browser.findElement(By.id('username')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('//input[@value="Login"]')).click();
        await waitForElement(By.xpath('//a[contains(@href, "/my-library")]'));
    }

    async function loginOIDC(username, password, alreadyAuthenticated = false) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/login`);
        await browser.sleep(2000);

        await browser.wait(until.elementLocated(By.xpath(`//a[contains(., "${cloudronName}") or contains(., "Cloudron")]`)), TEST_TIMEOUT);
        await browser.findElement(By.xpath('//a[contains(., "${cloudronName}") or contains(., "Cloudron")]')).click();
        await browser.sleep(2000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }
        await browser.sleep(20000);
        await waitForElement(By.xpath('//a[contains(@href, "/my-library")]'));
    }

    async function closeAccountSetupDialog() {
        await browser.get('https://' + app.fqdn);

        await waitForElement(By.xpath('//span[contains(text(), "show me this anymore")]'));
        await browser.findElement(By.xpath('//span[contains(text(), "show me this anymore")]')).click();
        await browser.findElement(By.xpath('//a[contains(text(), "Set up")]')).click();
        await browser.sleep(3000);
    }

    async function completeSetup() {
        let button;

        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(2000);

        const [error] = await safe(waitForElement(By.xpath('//a[contains(text(), "Configure my instance")]')));
        if (error) return; // sometimes it doesn't appear, maybe it's cached in local storage
        await browser.findElement(By.xpath('//a[contains(text(), "Configure my instance")]')).click(); // this opens a new window

        await browser.sleep(2000);
        await closeTab();
    }

    async function uploadVideo() {
        await browser.get(`https://${app.fqdn}/videos/upload#upload`);
        await browser.sleep(3000);
        await browser.wait(until.elementLocated(By.id('videofile')), TEST_TIMEOUT); // do not do element visible check . it fails, no clue why
        await browser.findElement(By.id('videofile')).sendKeys(path.resolve(__dirname, './Cloudron Test Video.mp4'));
        console.log('waiting 10 seconds for upload');
        await browser.sleep(10000); // wait for upload

        await waitForElement(By.xpath('//div[@class="submit-container"]//span[contains(.,"Publish")]'));
        let button = browser.findElement(By.xpath('//div[@class="submit-container"]//span[contains(.,"Publish")]'));
        await browser.executeScript('arguments[0].scrollIntoView(false)', button);
        await browser.sleep(2000);
        await button.click();
        await browser.sleep(2000);
    }

    async function videoExists() {
        await browser.get('https://' + app.fqdn + '/my-account/videos');
        await waitForElement(By.xpath('//a[contains(@title, "Cloudron Test Video")]'));
    }

    async function closeTab() {
        const handles = await browser.getAllWindowHandles();
        await browser.switchTo().window(handles[1]);
        await browser.close();
        await browser.switchTo().window(handles[0]);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // No SSO
    it('install app (no sso)', async function () {
        execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS);
        await sleep(10000); // takes a bit to create root user in background
    });

    it('can get app information', getAppInfo);

    it('can login (no sso)', login.bind(null, 'root', 'changeme'));
    it('can complete setup', completeSetup);
    it('can logout', clearCache);

    it('uninstall app (no sso)', async function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // SSO
    it('install app', async function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        await sleep(40000); // takes a bit to create root user in background
    });

    it('can get app information', getAppInfo);
    it('can root login', login.bind(null, 'root', 'changeme'));
    it('can complete setup', completeSetup);
    it('can upload video', uploadVideo);
    it('video exists', videoExists);
    it('logout', clearCache);

    it('can get app information', getAppInfo);

    it('can OIDC login', loginOIDC.bind(null, username, password));
    it('can close account setup dialog', closeAccountSetupDialog);
    it('logout', clearCache);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can root login', login.bind(null, 'root', 'changeme'));
    it('video exists', videoExists);
    it('logout', clearCache);

    it('can OIDC login', loginOIDC.bind(null, username, password));
    it('logout', clearCache);

    it('can restart app', function () {
        execSync('cloudron restart --app ' + app.id);
    });
    it('can root login', login.bind(null, 'root', 'changeme'));
    it('video exists', videoExists);
    it('logout', clearCache);

    it('can OIDC login', loginOIDC.bind(null, username, password));
    it('logout', clearCache);

    // this is not supported for federation
    it('move to different location', function () { execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can root login', login.bind(null, 'root', 'changeme'));
    it('video exists', videoExists);
    it('logout', clearCache);

    it('can OIDC login', loginOIDC.bind(null, username, password));
    it('logout', clearCache);

    it('uninstall app', async function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', async function () {
        execSync('cloudron install --appstore-id org.joinpeertube.cloudronapp --location ' + LOCATION, EXEC_ARGS);
        await sleep(10000); // takes a bit to create root user in background
    });

    it('can get app information', getAppInfo);
    it('can root login', login.bind(null, 'root', 'changeme'));
    it('can complete setup', completeSetup);
    it('can upload video', uploadVideo);
    it('video exists', videoExists);
    it('logout', clearCache);

    it('can update', function () { execSync('cloudron update --app ' + app.id, EXEC_ARGS); });
    it('can root login', login.bind(null, 'root', 'changeme'));
    it('video exists', videoExists);
    it('logout', clearCache);

    it('can OIDC login', loginOIDC.bind(null, username, password));
    it('can close account setup dialog', closeAccountSetupDialog);
    it('logout', clearCache);

    it('uninstall app', async function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
