FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code/server /app/pkg
WORKDIR /app/code/server

RUN apt update && apt install -y prosody && rm -rf /var/cache/apt /var/lib/apt/lists

ARG NODE_VERSION=20.15.1
RUN mkdir -p /usr/local/node-${NODE_VERSION} && curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH /usr/local/node-${NODE_VERSION}/bin:$PATH

# renovate: datasource=github-releases depName=Chocobozzz/PeerTube versioning=semver extractVersion=^v(?<version>.+)$
ARG PEERTUBE_VERSION=7.0.1

# install peertube. the chown is required for some reason for ffmpeg. it fails with EROFS without it when transcoding
RUN curl -sL https://github.com/Chocobozzz/PeerTube/releases/download/v${PEERTUBE_VERSION}/peertube-v${PEERTUBE_VERSION}.tar.xz | tar --strip-components 1 -Jxvf - && \
    yarn install --production --pure-lockfile

RUN npm install -g @peertube/peertube-cli

# the json is created and managed by web ui. it overrides the yaml file (https://github.com/lorenwest/node-config/wiki/Configuration-Files)
RUN ln -sf /app/data/production.yaml /app/code/server/config/production.yaml && \
    ln -sf /app/data/local-production.json /app/code/server/config/local-production.json

# youtube-dl
# renovate: datasource=github-releases depName=ytdl-org/youtube-dl versioning=loose extractVersion=^(?<version>.+)$
ARG YOUTUBE_DL_VERSION=2021.12.17

RUN curl -L https://github.com/ytdl-org/youtube-dl/releases/download/${YOUTUBE_DL_VERSION}/youtube-dl -o /usr/local/bin/youtube-dl && chmod +x /usr/local/bin/youtube-dl

# required when installing plugins/themes that use node-gyp
RUN rm -rf /home/cloudron/.cache && ln -s /run/peertube/cache /home/cloudron/.cache && \
    rm -rf /home/cloudron/.npm && ln -s /run/peertube/npm /home/cloudron/.npm

COPY production.yaml.example start.sh /app/pkg/

ENV NODE_ENV production
ENV NODE_CONFIG_DIR /app/code/server/config

RUN printf '#!/bin/bash\n\ncd /app/code/cli && node dist/server/tools/peertube.js $@\n' > /usr/bin/peertube && chmod +x /usr/bin/peertube

# remove with next releases
RUN printf '#!/bin/bash\n\ncd /app/code/server && NODE_ENV=production node dist/scripts/migrations/peertube-6.3.js $@\n' > /usr/bin/migrate && chmod +x /usr/bin/migrate

# add nginx config
RUN rm /etc/nginx/sites-enabled/*
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log
RUN ln -sf /run/peertube-nginx.conf /etc/nginx/sites-enabled/peertube.conf
COPY nginx/readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf
COPY nginx/peertube.conf /app/pkg/peertube-nginx.conf

# supervisor
COPY supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf

CMD [ "/app/pkg/start.sh" ]

